# Teste React Native - Orbit Labs

O teste consiste em desenvolver uma tela que contém um Header com algumas mensagens, e montar uma lista com usuários, sendo que podemos ver sua foto, nome e a opção de seguir ou deixar de seguir.

## Organização de pastas

- src: Pasta principal onde fica boa do código desenvolvido.

- assets: Possui uma pasta fonts para colocar as fontes da aplicação e images para colocar qualquer imagem necessária. (Nesse caso só fui usada a de fonts).

- components: Pasta usada para criar componentes mais genéricos que podem ser usadas em outras páginas.

- pages: Onde colocamos componentes que serão telas da aplicação, fazendo o import no arquivo de rotas.

- services: Usada para armazenar funções que consomem serviços fora da aplicação, um bom exemplo é a configuração do axios para se comunicar com uma API.

- store: Usada para armazenar funções do redux, desde das actions creators, até os reducers.

  - ducks: Foi utilizado o duck pattern para criar a estrutura do redux, basicamente colocamos os actions types, actions creators e reducer em um único arquivo.

- styles: Para colocar estilos mais globais da aplicação, possui um arquivo com as cores, e alguns valores de base para padding, margin, border radius e etc.

- **tests**: Pasta onde ficam armazenados os testes da aplicação.

  - components: Testes para os componentes mais genéricos.
  - reducers: Para testar apenas os reducers da aplicação.
  - pages: Testes das páginas.

## Arquivos de configuração

- routes.js: É o arquivo onde configuramos as rotas usando react-navigation, caso a aplicação tenha mais de uma página (tela), devemos registrar lá.

- setupTests.js: Para configurar o Jest e Enzyme para poder escrever testes, sendo necessário criar um arquivo do jest para carregar essas configurações, ou pode colocar no package.json dentro da chave "jest".

## Algumas observações

- No primeiro momento esse teste consiste de apenas uma tela, e não seria necessário o uso do Redux para isso, mas foi usado para mostrar os conhecimentos nesse assunto e caso o teste tivesse mais telas e precisasse compartilhar o estado, o Redux com certeza ajuda.

- Foi usado um arquivo json para simular uma requisição, basicamente esse arquivo foi importado e colocou um setTimout para simular o tempo de requisição e resposta e logo depois o resultado é mandado pro Redux e a página Main que está conectada ao Redux recebe a props e renderiza por meio da FlatList uma lista com os usuários.

- No Redux criamos um estado inicial vazio, e possui três action, "setUsers", "followUser" e "unfollowUser".

  - setUsers: recebe um array de usuários e adiciona ao estado, na requisição vem apenas o nome e a imagem do usuário, então é colocado uma propriedado "follow" com valor false para indicar que não está seguindo esse usuário.
  - followUser: Busca um usuário pelo nome e atualiza a propriedade "follow" para true.
  - unfollowUser: Busca um usuário pelo nome e atualiza a propriedade "follow" para falso.

- Dependendo do valor da propriedade "follow" do usuário, o botão é renderizado de maneira diferente, mudando a cor e o texto.

- Foi utilizado o PropTypes para definir as estruturas de cada props que o componente pode ter.

- Foi utilizado o eslint e prettier para padronizar o código.

## Rodando os testes

1. Instale as depedências com o comando "yarn install".

2. Basta rodar o comando "yarn run test".

## Rodando a aplicação

1. Caso não tenha instalado as depedênciass rode o comando "yarn install".

2. react-native run-android ou react-native run-ios.
