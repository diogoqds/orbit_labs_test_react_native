import { StyleSheet } from 'react-native';
import { colors, metrics } from '~/styles';

const styles = StyleSheet.create({
  container: {
    padding: 5,
  },
  body: {
    borderColor: colors.primary,
    borderWidth: 5,
    padding: metrics.basePadding / 2.5,
  },
  title: {
    fontSize: 40,
    textTransform: 'uppercase',
    fontWeight: 'bold',
    color: colors.secondary,
    fontFamily: 'Barlow',
  },
  infoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  messageContainer: {
    width: '65%',
  },
  message: {
    fontSize: 18,
    color: colors.primary,
    fontWeight: 'bold',
    fontFamily: 'Barlow',
  },
  buttonContainer: {
    width: '35%',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  button: {
    backgroundColor: colors.primary,
    paddingVertical: metrics.baseMargin / 2,
    paddingHorizontal: metrics.baseMargin / 2,
  },
  textButton: {
    fontSize: 14,
    color: colors.white,
    textTransform: 'uppercase',
    fontWeight: 'bold',
    fontFamily: 'Barlow',
  },
});

export default styles;
