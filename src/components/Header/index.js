import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

const Header = () => (
  <View testID="container" style={styles.container}>
    <View style={styles.body}>
      <Text testID="title" style={styles.title}>
        Excelente!
      </Text>

      <View style={styles.infoContainer}>
        <View style={styles.messageContainer}>
          <Text testID="message" style={styles.message}>
            Para aproveitar ao máximo, se conecte com seus amigos
          </Text>
        </View>

        <View style={styles.buttonContainer}>
          <TouchableOpacity testID="button" style={styles.button} onPress={() => {}}>
            <Text style={styles.textButton}>continuar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  </View>
);

export default Header;
