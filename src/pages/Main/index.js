import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  View,
  Image,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UserActions from '~/store/ducks/users';
import Header from '../../components/Header/index';
import styles from './styles';
import feedJson from '../../../feed.json';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    try {
      const response = feedJson;
      const { setUsers } = this.props;
      setTimeout(() => {
        setUsers(response);
      }, 1500);
    } catch (error) {
      Alert.alert('Erro na requisição');
    } finally {
      setTimeout(() => {
        this.setState({ loading: false });
      }, 1000);
    }
  }

  handleClick = (userName, follow) => {
    const { followUser, unfollowUser } = this.props;
    if (follow) {
      unfollowUser(userName);
    } else {
      followUser(userName);
    }
  };

  renderUsers = () => {
    const { users } = this.props;
    return (
      <FlatList
        data={users}
        keyExtractor={item => String(item.name)}
        renderItem={({ item: user, index }) => (
          <View style={[styles.userContainer, index % 2 === 0 ? styles.userContainerOdd : {}]}>
            <View style={styles.avatarContainer}>
              <Image source={{ uri: user.image }} style={styles.avatar} />
              <Text style={styles.text}>{user.name}</Text>
            </View>

            <View style={styles.actionContainer}>
              <TouchableOpacity
                style={[styles.button, user.follow ? styles.secondaryButton : styles.primaryButton]}
                onPress={() => this.handleClick(user.name, user.follow)}
              >
                <Text style={styles.textButton}>{user.follow ? 'Seguindo' : 'Seguir'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      />
    );
  };

  render() {
    const { loading } = this.state;
    return (
      <ScrollView testID="container">
        <Header />
        {loading ? <ActivityIndicator size="large" /> : this.renderUsers()}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  users: state.users.data,
});

const mapDispatchToProps = dispatch => bindActionCreators(UserActions, dispatch);

Main.propTypes = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      image: PropTypes.string,
      follow: PropTypes.bool,
    }),
  ).isRequired,
  setUsers: PropTypes.func.isRequired,
  followUser: PropTypes.func.isRequired,
  unfollowUser: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main);
