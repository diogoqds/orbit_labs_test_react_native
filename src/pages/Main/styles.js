import { StyleSheet } from 'react-native';
import { metrics, colors } from '~/styles';

const styles = StyleSheet.create({
  userContainer: {
    flexDirection: 'row',
    // marginBottom: metrics.baseMargin,
    backgroundColor: 'white',
    justifyContent: 'space-between',
    height: 100,
  },
  userContainerOdd: {
    backgroundColor: 'rgba(20, 73, 191, 0.1)',
  },
  avatarContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    width: 80,
    height: 80,
    marginHorizontal: metrics.baseMargin,
  },
  text: {
    fontSize: 16,
    color: colors.black,
    fontFamily: 'Barlow',
  },
  actionContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: metrics.baseMargin,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: metrics.basePadding / 4,
    width: 100,
  },
  primaryButton: {
    backgroundColor: colors.primary,
  },
  secondaryButton: {
    backgroundColor: colors.secondary,
  },
  textButton: {
    color: colors.white,
    textTransform: 'uppercase',
    fontSize: 16,
    fontFamily: 'Barlow',
  },
});

export default styles;
