import Immutable from 'seamless-immutable';

const INITIAL_STATE = Immutable({
  data: [],
});

const Types = {
  SET_USERS: 'users/SET_USERS',
  FOLLOW_USER: 'users/FOLLOW_USER',
  UNFOLLOW_USER: 'users/UNFOLLOW_USER',
};

const Creators = {
  setUsers: data => ({
    type: Types.SET_USERS,
    payload: { data },
  }),
  followUser: name => ({
    type: Types.FOLLOW_USER,
    payload: { name },
  }),
  unfollowUser: name => ({
    type: Types.UNFOLLOW_USER,
    payload: { name },
  }),
};

export function reducer(state = INITIAL_STATE, { type, payload }) {
  switch (type) {
    case Types.SET_USERS:
      return state.merge({
        data: payload.data.map(user => ({ ...user, follow: false })),
      });
    case Types.FOLLOW_USER:
      return state.merge({
        data: state.data.map(user => (user.name !== payload.name ? user : { ...user, follow: true })),
      });
    case Types.UNFOLLOW_USER:
      return state.merge({
        data: state.data.map(user => (user.name !== payload.name ? user : { ...user, follow: false })),
      });
    default:
      return state;
  }
}

export const UserTypes = Types;
export default Creators;
