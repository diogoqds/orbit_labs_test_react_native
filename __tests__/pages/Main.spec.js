import React from 'react';
import { shallow } from 'enzyme';
import createMockStore from 'redux-mock-store';

import Main from '../../src/pages/Main';

const mockStore = createMockStore();

const INITIAL_STATE = {
  users: { data: [] },
};

const store = mockStore(INITIAL_STATE);

describe('Main page', () => {
  it('render without error', () => {
    const wrapper = shallow(<Main store={store} />)
      .dive()
      .dive();
    const container = wrapper.findWhere(node => node.prop('testID') === 'container');
    expect(container.exists()).toBe(true);
  });
});
