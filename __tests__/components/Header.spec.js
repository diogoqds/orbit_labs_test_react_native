import React from 'react';
import { shallow } from 'enzyme';
import Header from '../../src/components/Header';

describe('Header component', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<Header />);
  });

  it('render without error', () => {
    const containerWrapper = wrapper.findWhere(node => node.prop('testID') === 'container');

    expect(containerWrapper.exists()).toBe(true);
  });

  it('render title', () => {
    const titleWrapper = wrapper.findWhere(node => node.prop('testID') === 'title');
    expect(titleWrapper.exists()).toBe(true);
  });

  it('render message', () => {
    const messageWrapper = wrapper.findWhere(node => node.prop('testID') === 'message');
    expect(messageWrapper.exists()).toBe(true);
  });

  it('render button', () => {
    const buttonWrapper = wrapper.findWhere(node => node.prop('testID') === 'button');
    expect(buttonWrapper.exists()).toBe(true);
  });
});
