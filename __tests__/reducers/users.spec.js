import UserActions, { reducer as userReducer } from '~/store/ducks/users';
import Immutable from 'seamless-immutable';

describe('User Reducer', () => {
  it('should be able to add users', () => {
    const newUser = {
      name: 'Tyrion',
      image: 'http://assets.viewers-guide.hbo.com/large59944e7d84b41@2x.jpg',
    };
    const state = userReducer(Immutable({ data: [] }), UserActions.setUsers([newUser]));
    expect(state.data[0]).toEqual({ ...newUser, follow: false });
  });

  it('should be able to follow user', () => {
    const users = [
      {
        name: 'Tyrion',
        image: 'http://assets.viewers-guide.hbo.com/large59944e7d84b41@2x.jpg',
        follow: false,
      },
      {
        name: 'Daenerys',
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMG4XduQp4S_UE2krAX6Up0IIm0-2QGNhxcN00JUKvU0JRSI4I',
        follow: false,
      },
    ];
    const state = userReducer(Immutable({ data: users }), UserActions.followUser('Daenerys'));
    expect(state.data[1]).toEqual({ ...users[1], follow: true });
  });

  it('should be able to unfollow user', () => {
    const users = [
      {
        name: 'Tyrion',
        image: 'http://assets.viewers-guide.hbo.com/large59944e7d84b41@2x.jpg',
        follow: true,
      },
      {
        name: 'Daenerys',
        image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMG4XduQp4S_UE2krAX6Up0IIm0-2QGNhxcN00JUKvU0JRSI4I',
        follow: false,
      },
    ];
    const state = userReducer(Immutable({ data: users }), UserActions.unfollowUser('Tyrion'));
    expect(state.data[0]).toEqual({ ...users[0], follow: false });
  });
});
